# Flag Randomizer

Flag Randomizer for 4chan.
Supports 4chanX, as well the native 4chan extension and default post reply box.

If you want to contribute, fork this repository and start hacking away.
If you want a list of things to work on here's some ideas:

- The Flag chooser element at the bottom of the navigation page, doesn't
    shrink/expand alongside the other elements. This should be fixed.
- Some people are complaining about not wanting a purely random randomizer,
    and would like for some way to specify their custom flag choices.
    - This can be done with a button that attaches to the quick reply box
        and long post box, that randomizes the flag on click.
    - Alternatively you could write up a feature to import/export JSON lists
        and be able to specify your own custom list with a "key" that the
        user will be able to select in the drop down menu.

## Installation

Make sure you have ViolentMonkey installed.
Install it here for [Firefox](https://addons.mozilla.org/en-US/firefox/addon/violentmonkey/)
or here for [Chrome](https://chrome.google.com/webstore/detail/violent-monkey/jinjaccalgkegednnccohejagnlnfdag).

Then you can install the userscript
here from the [repository](https://gitlab.com/script-anon/flagrandomizer/-/raw/master/FlagRandomizer.user.js)
or [here](https://ponepaste.org/6796) from the paste.
Note that if you install from the paste you should navigate to the ViolentMonkey dashboard first and then
click to "Install From URL".
